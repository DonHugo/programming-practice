#+TITLE: Summary
#+PROPERTY: header-args :session :results output :exports both
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usemintedstyle{paraiso-light}

* Introduction & Setup

#+begin_src emacs-lisp
  ;; Latex code highlighting
  (setq org-latex-listings 'minted
        org-latex-packages-alist '(("" "minted")))
#+end_src

#+RESULTS:

This file tries to summarize the exercises of the book "R for Data Science".
_Required packages_ (installed before):

 - tidyverse
 - Datasets:
   - nycflights13
   - gapminder
   - Lahman
     
*IMPORTANT* for graphics: filenames need to be distinct, otherwise one always
gets the same one or we use this nifty little snippet: =:file (org-babel-temp-file "./plot-" ".png")=
     
#+BEGIN_SRC R
library("tidyverse")
#+END_SRC

#+RESULTS:
: ── [1mAttaching packages[22m ─────────────────────────────────────── tidyverse 1.2.1 ──
: [32m✔[39m [34mggplot2[39m 3.1.0       [32m✔[39m [34mpurrr  [39m 0.3.2
: [32m✔[39m [34mtibble [39m 2.1.1       [32m✔[39m [34mdplyr  [39m 0.8.0.[31m1[39m
: [32m✔[39m [34mtidyr  [39m 0.8.3       [32m✔[39m [34mstringr[39m 1.4.0
: [32m✔[39m [34mreadr  [39m 1.3.1       [32m✔[39m [34mforcats[39m 0.3.0
: ── [1mConflicts[22m ────────────────────────────────────────── tidyverse_conflicts() ──
: [31m✖[39m [34mdplyr[39m::[32mfilter()[39m masks [34mstats[39m::filter()
: [31m✖[39m [34mdplyr[39m::[32mlag()[39m    masks [34mstats[39m::lag()

* Ch1: Data Visualization with ggplot2

** First steps
1. Run =ggplot(data = mpg)=. What do you see?
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
   ggplot(data = mpg)
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-nK0Ldg.png]]

   Nothing? 
   
2. How many rows are in mtcars ? How many columns?
   #+BEGIN_SRC R :results output 
   str(mtcars)
   #+END_SRC

   #+RESULTS:
   #+begin_example
   'data.frame':	32 obs. of  11 variables:
    $ mpg : num  21 21 22.8 21.4 18.7 18.1 14.3 24.4 22.8 19.2 ...
    $ cyl : num  6 6 4 6 8 6 8 4 4 6 ...
    $ disp: num  160 160 108 258 360 ...
    $ hp  : num  110 110 93 110 175 105 245 62 95 123 ...
    $ drat: num  3.9 3.9 3.85 3.08 3.15 2.76 3.21 3.69 3.92 3.92 ...
    $ wt  : num  2.62 2.88 2.32 3.21 3.44 ...
    $ qsec: num  16.5 17 18.6 19.4 17 ...
    $ vs  : num  0 0 1 1 0 1 0 1 1 1 ...
    $ am  : num  1 1 1 0 0 0 0 0 0 0 ...
    $ gear: num  4 4 4 3 3 3 3 4 4 4 ...
    $ carb: num  4 4 1 1 2 1 4 2 2 4 ...
   #+end_example

3. What does the =drv= variable describe? Read the help for =?mpg= to find out.
   #+BEGIN_SRC R :results output
   ###?mpg
   #+END_SRC

   #+RESULTS:
   
   Needs to be commented out as it crashes emacs.
   However, the =drv= variable describes if the front, back or all 4 wheels are used.
   
4. Make a scatterplot of =hwy= versus =cyl=.
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
   ggplot(data = mpg, aes(x = cyl, y = hwy)) + geom_point()
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-39C0Ki.png]]

5. What happens if you make a scatterplot of =class= versus =drv= ?
   Why is the plot not useful?
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
   ggplot(data = mpg, aes(x = class, y = drv)) + geom_point()
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-VJtJgy.png]]

   This plot describes which wheels are used per class (e.g. back wheels for 2seaters). Does not to seem that interesting.
   
** Aesthetic mappings
1. What’s gone wrong with this code? Why are the points not blue?
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
ggplot(data = mpg) + 
  geom_point(mapping = aes(x = displ, y = hwy, color = "blue"))
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-TGQjeX.png]]

   "Blue" is the name of the plotted variable. The color should have been assigned outside of the =aes()= call
   
2. Which variables in =mpg= are categorical? Which variables are continuous? (Hint: type =?mpg= to read the documentation for the dataset.) How can you see this information when you run =mpg=?
   #+BEGIN_SRC R :results output
   str(mpg)
   #+END_SRC

   #+RESULTS:
   #+begin_example
   Classes ‘tbl_df’, ‘tbl’ and 'data.frame':	234 obs. of  11 variables:
    $ manufacturer: chr  "audi" "audi" "audi" "audi" ...
    $ model       : chr  "a4" "a4" "a4" "a4" ...
    $ displ       : num  1.8 1.8 2 2 2.8 2.8 3.1 1.8 1.8 2 ...
    $ year        : int  1999 1999 2008 2008 1999 1999 2008 1999 1999 2008 ...
    $ cyl         : int  4 4 4 4 6 6 6 4 4 4 ...
    $ trans       : chr  "auto(l5)" "manual(m5)" "manual(m6)" "auto(av)" ...
    $ drv         : chr  "f" "f" "f" "f" ...
    $ cty         : int  18 21 20 21 16 18 18 18 16 20 ...
    $ hwy         : int  29 29 31 30 26 26 27 26 25 28 ...
    $ fl          : chr  "p" "p" "p" "p" ...
    $ class       : chr  "compact" "compact" "compact" "compact" ...
   #+end_example

3. Map a continuous variable to color , size , and shape . How do these
    aesthetics behave differently for categorical versus continuous variables?
#+BEGIN_SRC R :results graphics :file (org-babel-temp-file "./plot-" ".png")
  ggplot(data = mpg) +
    geom_point(mapping = aes(x = displ, y = hwy, color = displ, size = displ))
#+END_SRC

#+RESULTS:
[[file:/tmp/babel-3oQ0S7/plot-PI2SqC.png]]
A continuous variable cannot be mapped to a shape.

4. What happens if you map the same variable to multiple aesthetics?
   See above.

5. What does the stroke aesthetic do? What shapes does it work with? (Hint: use
=?geom_point=.)

It modifies the line of an object which has a fill and a border.

#+BEGIN_SRC R :results graphics :file (org-babel-temp-file "./plot-" ".png")
  ggplot(data = mpg) +
    geom_point(mapping = aes(x = displ, y = hwy, stroke = cyl))
#+END_SRC

#+RESULTS:
[[file:/tmp/babel-3oQ0S7/plot-LKsXJC.png]]


6. What happens if you map an aesthetic to something other than a variable name,
like =aes(color = displ < 5)=?
#+BEGIN_SRC R :results graphics :file (org-babel-temp-file "./plot-" ".png")
  ggplot(data = mpg) +
    geom_point(mapping = aes(x = displ, y = hwy, color = displ < 5))
#+END_SRC

#+RESULTS:
[[file:/tmp/babel-3oQ0S7/plot-yVpVUD.png]]

** Facets
1. What happens if you facet on a continuous variable?
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg) + geom_point(aes(x = displ, y = hwy)) + facet_wrap(~ displ)
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-VoFoZX.png]]

2. What do the empty cells in a plot with =facet_grid(drv ~ cyl)= mean? How do
   they relate to this plot?
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg) + geom_point(mapping = aes(x = drv, y = cyl))
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-4V5BU1.png]]

3. What plots does the following code make? What does =.= do?
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg) +
       geom_point(mapping = aes(x = displ, y = hwy)) +
       facet_grid(drv ~ .)
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-Rfjp1e.png]]

   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg) +
       geom_point(mapping = aes(x = displ, y = hwy)) +
       facet_grid(. ~ cyl)
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-YHlNXH.png]]

4. Take the first faceted plot in this section:
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg) +
       geom_point(mapping = aes(x = displ, y = hwy)) +
       facet_wrap(~ class, nrow = 2)
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-Rwv5GG.png]]

   What are the advantages to using faceting instead of the color aesthetic?
   What are the disadvantages? How might the balance change if you had a larger
   dataset?
5. Read =?facet_wrap=. What does =nrow= do? What does =ncol= do? What other options
   control the layout of the individual panels? Why doesn’t =facet_grid()= have
   =nrow= and =ncol= variables?
6. When using =facet_grid()= you should usually put the variable with more unique
   levels in the columns. Why?

** Geometric Objects
1. What geom would you use to draw a line chart? A boxplot? A histogram? An area
   chart?
   - =geom_line=
   - =geom_boxplot=
   - =geom_histogram=
   - =geom_area=

2. Run this code in your head and predict what the output will look like. Then,
   run the code in R and check your predictions:
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(
       data = mpg,
       mapping = aes(x = displ, y = hwy, color = drv)
     ) +
       geom_point() +
       geom_smooth(se = FALSE)
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-JUiRBg.png]]

3. What does =show.legend = FALSE= do? What happens if you remove it? Why do you
   think I used it earlier in the chapter?
   - It removes the legend from display.
     
4. What does the =se= argument to =geom_smooth()= do?
   - It toggles the display of the standard error.
    
5. Will these two graphs look different? Why/why not?
   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg, mapping = aes(x = displ, y = hwy)) +
       geom_point() +
       geom_smooth()
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-3AgxXa.png]]

   #+BEGIN_SRC R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot() +
       geom_point(
         data = mpg,
         mapping = aes(x = displ, y = hwy)
       ) +
       geom_smooth(
         data = mpg,
         mapping = aes(x = displ, y = hwy)
       )
   #+END_SRC

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-Uvf4Nh.png]]

6. Re-create the R code necessary to generate the following graphs.
   #+begin_src R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg, aes(x = displ, y = hwy)) +
       geom_point() +
       geom_smooth(se = FALSE)
   #+end_src

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-xryV0y.png]]

   #+begin_src R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg, aes(x = displ, y = hwy)) +
       geom_point() +
       geom_smooth(aes(group = drv), se = FALSE)
   #+end_src

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-8Lknu1.png]]

   #+begin_src R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg, aes(x = displ, y = hwy, color = drv)) +
       geom_point() +
       geom_smooth(se = FALSE)
   #+end_src

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-XR5PnK.png]]

   #+begin_src R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg, aes(x = displ, y = hwy)) +
       geom_point(aes(color = drv)) +
       geom_smooth(se = FALSE)
   #+end_src

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-CC4dfK.png]]

   #+begin_src R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg, aes(x = displ, y = hwy)) +
       geom_point(aes(color = drv)) +
       geom_smooth(aes(lty = drv), se = FALSE)
   #+end_src

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-al7UtT.png]]

   #+begin_src R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = mpg, aes(x = displ, y = hwy)) +
       geom_point(aes(fill = drv), shape = 21, stroke = 1, color = "white")
   #+end_src

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-WkF9wp.png]]

** Statistical Transformations
1. What is the default geom associated with =stat_summary()=? How could you
   rewrite the previous plot to use that geom function instead of the stat
   function?
   - =geom_pointrange=
2. What does =geom_col()= do? How is it different to =geom_bar()=?
   - =geom_bar()= makes the height of the bar proportional to the
     number of cases in each group
   - =geom_col()= makes the heights of the bars to represent values in the data
3. Most geoms and stats come in pairs that are almost always used in
   concert. Read through the documentation and make a list of all the
   pairs. What do they have in common?
4. What variables does =stat_smooth()= compute? What parameters control its
   behavior?
   - =y= predicted value
   - =ymin= lower pointwise confidence interval around the mean
   - =ymax= upper pointwise confidence interval around the mean
   - =se= standard error
5. In our proportion bar chart, we need to set =group = 1=. Why? In other words
   what is the problem with these two graphs?

   #+begin_src R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = diamonds) +
       geom_bar(mapping = aes(x = cut, y = ..prop..))
   #+end_src

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-CHs72c.png]]

   #+begin_src R :results output graphics :file (org-babel-temp-file "./plot-" ".png")
     ggplot(data = diamonds) +
       geom_bar(mapping = aes(x = cut, fill = color, y = ..prop..))
   #+end_src

   #+RESULTS:
   [[file:/tmp/babel-3oQ0S7/plot-gzUNng.png]]
   - The proportions are computed groupwise, thus if =group = 1= is not set,
     there are no groups
