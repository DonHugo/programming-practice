#lang htdp/bsl

(define (string-last str)
  (if (string? str)
      (string-ith str (- (string-length str) 1))
      "Error, not a string"))