#lang htdp/bsl

(define (==> sunny friday)
  (or (and  sunny #false)
      (and friday #true)))