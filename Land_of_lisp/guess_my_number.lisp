;; Variables
(defparameter *small* 1)
(defparameter *big* 100)

;; Functions
(defun guess-my-number ()
  "Tries to guess the number of the user."
  ;; `ash' shifts this binary bits of a number to left or right. This will half
  ;; the search space -> binary search
  (ash (+ *small* *big*) -1))

(defun smaller ()
  "Gives `guess_my_number' the hint that the number is smaller."
  ;; `smaller' will reduce the `*big*' variable by one
  (setf *big* (1- (guess-my-number)))
  (guess-my-number))

(defun bigger ()
  "Gives `guess_my_number' the hint that the number is bigger."
  ;; Analog to `smaller'
  (setf *small* (1+ (guess-my-number)))
  (guess-my-number))

(defun start-over ()
  "Reset `*small*' and `*big*' and restart guessing game."
  (defparameter *small* 1)
  (defparameter *big* 100)
  (guess-my-number))
