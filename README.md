# Programming practice

My little playground. 

Contains exercises from 

- [Automate the Boring Stuff with Python](https://automatetheboringstuff.com)

- [How to Design Programs, Second Edition](https://htdp.org/2018-01-06/Book/)

- [R for Data Science](https://r4ds.had.co.nz) 

    > This contains a few topics from the DataCamp [Data Scientist with R Career
    Track](https://www.datacamp.com/tracks/data-scientist-with-r) which I would like
    to repeat and not forget.
    
- A bit of [exercism.io](https://exercism.io)
