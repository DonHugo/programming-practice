;; 5.1
(defun good-style (p)
  (let ((p (+ p 5)))
    (list 'result 'is p)))

(good-style 8)
;; => (RESULT IS 13)

;; 5.6
;; a)
(defun throw-die ()
  (let ((number (random 7)))
    ;; Do not return 0
    (if (= number 0)
        (+ number 1)
        number)))

(throw-die)
;; => 6 (3 bits, #x6, #o6, #b110)

;; b)
(defun throw-dice ()
  (list (throw-die) (throw-die)))

(throw-dice)
;; => (3 2)

;; c)
(defun snake-eyes-p (x)
  (when (and (= (first x) 1)
             (= (second x) 1))
    t))

(defun boxcars-p (x)
  (when (and (= (first x) 6)
             (= (second x) 6))
    t))

(snake-eyes-p (throw-dice))
;; => NIL
(boxcars-p (throw-dice))
;; => NIL

;; d)
(defun instant-loss-p (x)
  (let ((eye-sum (+ (first x) (second x))))
    (when (or (= eye-sum 2)
              (= eye-sum 3)
              (= eye-sum 12))
      t)))

(defun instant-win-p (x)
  (let ((eye-sum (+ (first x) (second x))))
    (when (or (= eye-sum 7)
              (= eye-sum 11))
      t)))

(instant-loss-p (throw-dice))
;; => NIL
(instant-win-p (throw-dice))
;; => NIL

;; e)
(defun say-throw (x)
  (let ((eye-sum (+ (first x) (second x))))
    (cond ((snake-eyes-p x) 'snake-eyes)
          ((boxcars-p x) 'boxcars)
          (t eye-sum))))

(say-throw (throw-dice))
;; => 8 (4 bits, #x8, #o10, #b1000)

;; f)
(defun craps ()
  (let* ((x (throw-dice))
         (d1 (first x))
         (d2 (second x)))
    (append (list 'throw d1 'and d2 '--)
            (cond ((instant-win-p x) (list (say-throw x) '-- 'you 'win))
                  ((instant-loss-p x) (list (say-throw x) '-- 'you 'lose))
                  (t (list 'your 'point 'is (say-throw x)))))))

(craps)
;; => (THROW 1 AND 5 -- YOUR POINT IS 6)

;; g)
(defun try-for-point (a)
  (let* ((x (throw-dice))
         (d1 (first x))
         (d2 (second x))
         (sum (+ d1 d2)))
    (append (list 'throw d1 'and d2 '--)
            (cond ((= a sum) (list sum '-- 'you 'win))
                  ((= sum 7) (list sum '-- 'you 'lose))
                  (t (list sum '-- 'throw 'again))))))

(try-for-point 5)
;; => (THROW 3 AND 1 -- 4 -- THROW AGAIN)
