;; 7.1
(defun add1 (n)
  "Add 1 to N."
  (+ n 1))

(mapcar #'add1 '(1 3 5 7 9))
;; => (2 4 6 8 10)

;; 7.2
(defvar daily-planet '((olsen jimmy 123-76-4535 cub-reporter)
                       (kent clark 089-52-6787 reporter)
                       (lane lois 951-26-1438 reporter)
                       (white perry 355-16-7439 editor)))
;; => DAILY-PLANET

(mapcar #'third daily-planet)
;; => (|123-76-4535| |089-52-6787| |951-26-1438| |355-16-7439|)

;; 7.3
(mapcar #'zerop '(2 0 3 4 0 -5 -6))
;; => (NIL T NIL NIL T NIL NIL)

;; 7.4
(defun greater-than-five-p (n)
  "Return T if N is greater than five."
  (> n 5))

(mapcar #'greater-than-five-p '(7 5 8 9 5 0 -1))
;; => (T NIL T T NIL NIL NIL)

;; 7.5
(lambda (n) (- n 7))
;; => #<FUNCTION (LAMBDA (N)) {52CBEFAB}>

;; 7.6
(funcall #'(lambda (n) (if (or (equal n t) (not n))
                      t
                      nil)) nil)
;; => T

;; 7.7
(defun flipper (l)
  "Flip the words 'up' and 'down' in L."
  (mapcar #'(lambda (n) (cond ((equal n 'up) 'down)
                         ((equal n 'down) 'up)
                         (t n))) l))

(flipper '(up down up up))
;; => (DOWN UP DOWN DOWN)

;; 7.8
(defun find-roughly (x k)
  "Find number in X which is within K +- 10."
  (find-if #'(lambda (entry) (and (>= entry (- k 10))
                             (<= entry (+ k 10)))) x))

(find-roughly '(1 3 40 500 15 9 7 2 0) 1)
;; => 1 (1 bit, #x1, #o1, #b1)
(find-roughly '(1 3 40 500 15 9 7 2 0) 40)
;; => 40 (6 bits, #x28, #o50, #b101000)

;; 7.9
(defun find-nested (l)
  "Return first non-empty list in L"
  (find-if #'(lambda (entry) (when entry entry)) l))

(find-nested '(() (2 3 4) nil (1)))
;; => (2 3 4)

;; 7.10
;; a)
(defvar note-table '((c 1)
                     (c-sharp 2)
                     (d 3)
                     (d-sharp 4)
                     (e 5)
                     (f 6)
                     (f-sharp 7)
                     (g 8)
                     (g-sharp 9)
                     (a 10)
                     (a-sharp 11)
                     (b 12)))

;; b)
(defun numbers (notes)
  "Transform note in NOTES into number."
  (mapcar #'(lambda (note)
              (second (assoc note note-table)))
          notes))

(numbers '(e d c d e e e))
;; => (5 3 1 3 5 5 5)

;; c)
(defun notes (numbers)
  "Transform number in NUMBERS into note."
  (mapcar #'(lambda (key)
              (car (find-if #'(lambda (entry)
                                (equal key (second entry)))
                            note-table)))
          numbers))

(notes '(5 3 1 3 5 5 5))
;; => (E D C D E E E)

;; e)
(defun raise (x notes)
  (mapcar #'(lambda (n) (+ n x)) notes))

(raise 5 '(5 3 1 3 5 5 5))
;; => (10 8 6 8 10 10 10)

;; f)
(defun normalize (numbers)
  (mapcar #'(lambda (n) (cond ((> n 12) (- n 12))
                         ((< n 1) (+ n 12))
                         (t n))) numbers))

(normalize '(6 10 13))
;; => (6 10 1)

;; g)
(defun transpose (n song)
  (notes (normalize (raise n (numbers song)))))

(transpose 5 '(e d c d e e e))
;; => (A G F G A A A)
(transpose 11 '(e d c d e e e))
;; => (D-SHARP C-SHARP B C-SHARP D-SHARP D-SHARP D-SHARP)
(transpose 12 '(e d c d e e e))
;; => (E D C D E E E)
(transpose -1 '(e d c d e e e))
;; => (D-SHARP C-SHARP B C-SHARP D-SHARP D-SHARP D-SHARP)

;; 7.11
(defun pick-numbers (l)
  (remove-if #'(lambda (x) (or (<= x 1) (>= x 5))) l))

(pick-numbers '(2 10 8 7 3 4 6))
;; => (2 3 4)

;; 7.12
(defun count-the (sentence)
  (length (remove-if-not #'(lambda (x) (equal x 'the)) sentence)))

(count-the '(the horse jumps around the field))
;; => 2 (2 bits, #x2, #o2, #b10)

;; 7.13
(defun pick-list (l)
  (remove-if-not #'(lambda (x) (= (length x) 2)) l))

(pick-list '((2 3 4) (a b) (5 6) (a c b d)))
;; => ((A B) (5 6))

;; 7.15
;; a)
(defun rank (card)
  (first card))

(rank '(2 clubs))
;; => 2 (2 bits, #x2, #o2, #b10)

(defun suit (card)
  (second card))

(suit '(2 clubs))
;; => CLUBS

;; b)
(defvar my-hand '((3 hearts)
                  (5 clubs)
                  (2 diamonds)
                  (4 diamonds)
                  (ace spades)))

(defun count-suit (suit hand)
  (length (remove-if-not #'(lambda (x) (equal (suit x) suit))
                         hand)))

(count-suit 'diamonds my-hand)
;; => 2 (2 bits, #x2, #o2, #b10)

;; c)
(defvar colors '((clubs black)
                 (diamonds red)
                 (hearts red)
                 (spades black)))

(defun color-of (card)
  (second (assoc (suit card) colors)))

(color-of '(2 clubs))
;; => BLACK
(color-of '(6 hearts))
;; => RED

;; d)
(defun first-red (hand)
  (first (remove-if-not #'(lambda (x) (equal (color-of x) 'red))
                        hand)))

(first-red '((2 spades)
             (5 hearts)
             (4 clubs)
             (3 hearts)
             (ace spades)))
;; => (5 HEARTS)
(first-red '((2 spades)
             (5 spades)
             (4 clubs)
             (3 spades)
             (ace spades)))
;; => NIL

;; e)
(defun black-cards (hand)
  (remove-if-not #'(lambda (x) (equal (color-of x) 'black)) hand))

(black-cards '((2 spades)
               (5 hearts)
               (4 clubs)
               (3 hearts)
               (ace spades)))

;; f)
(defun what-ranks (suit hand)
  (mapcar #'rank (remove-if-not #'(lambda (x) (equal suit (suit x)))
                                hand)))

(what-ranks 'diamonds my-hand)
;; => (2 4)

;; g)
(defvar all-ranks '(2 3 4 5 6 7 8 9 10 jack queen kind ace))

(defun higher-rank-p (card1 card2)
  "Return true if CARD1 is higher than CARD2."
  (when (member card1 (member card2 all-ranks))
    t))

(higher-rank-p 3 'jack)
;; => NIL
(higher-rank-p 'queen 'jack)
;; => T

;; h)
(defun high-card (hand)
  "Return highest ranked card in HAND."
  (assoc (first (sort (reduce #'append (mapcar #'(lambda (x) (what-ranks x hand)) (mapcar #'first colors))) #'higher-rank-p)) hand))

(high-card my-hand)
;; => (ACE SPADES)

;; 7.16
(remove-duplicates (reduce #'append '((a b c) (c d a) (f b d) (g))))
;; => (C A F B D G)

;; 7.17
(defun length-all-lists (lists)
  "Sum of the lengths of all sublists."
  (reduce #'+ (mapcar #'length lists)))

(length-all-lists '((a b c) (c d a) (f b d) (g)))
;; => 10 (4 bits, #xA, #o12, #b1010)

;; 7.19
(defun all-odd (l)
  "Return true if all elements in L are odd."
  (every #'oddp l))

(all-odd '(1 2 3 4 5))
;; => NIL
(all-odd '(1 3 5))
;; => T

;; 7.20
(defun none-odd (l)
  "Return true if no element in L is odd."
  (every #'(lambda (x) (not (oddp x))) l))

(none-odd '(1 2 3 4 5))
;; => NIL
(none-odd '(1 4 5))
;; => NIL
(none-odd '(2 4 6))
;; => T

;; 7.21
(defun not-all-odd (l)
  "Return true if not all elements in L are odd."
  (not (every #'oddp l)))

(not-all-odd '(1 4 5))
;; => T

;; 7.29

(defvar database
  '((b1 shape brick)
    (b1 color green)
    (b1 size small)
    (b1 supported-by b2)
    (b1 supported-by b3)
    (b2 shape brick)
    (b2 color red)
    (b2 size small)
    (b2 supports b1)
    (b2 left-of b3)
    (b3 shape brick)
    (b3 color red)
    (b3 size small)
    (b3 supports b1)
    (b3 right-of b2)
    (b4 shape pyramid)
    (b4 color blue)
    (b4 size large)
    (b4 supported-by b5)
    (b5 shape cube)
    (b5 color green)
    (b5 size large)
    (b5 supports b4)
    (b6 shape brick)
    (b6 color purple)
    (b6 size large)))

;; a)
(defun match-element (sym1 sym2)
  (cond ((equal sym1 sym2))
        ((equal sym2 '?))))

(match-element 'red 'red)
;; => T
(match-element 'red '?)
;; => T
(match-element 'red 'blue)
;; => NIL

;; b)
(defun match-triple (assertion pattern)
  (every #'identity (mapcar #'match-element assertion pattern)))

(match-triple '(b2 color red) '(b2 color ?))
;; => T
(match-triple '(b2 color red) '(b1 color green))
;; => NIL

;; c)
(defun fetch (pattern)
  (remove-if-not #'(lambda (entry) (match-triple entry pattern)) database))

(fetch '(b2 color ?))
;; => ((B2 COLOR RED))
(fetch '(? supports b1))
;; => ((B2 SUPPORTS B1) (B3 SUPPORTS B1))

;; d)
;; What shape is block B4?
(fetch '(b4 shape ?))
;; => ((B4 SHAPE PYRAMID))
;; Which blocks are bricks?
(fetch '(? shape brick))
;; => ((B1 SHAPE BRICK) (B2 SHAPE BRICK) (B3 SHAPE BRICK) (B6 SHAPE BRICK))
;; What relation is block B2 to block B3?
(fetch '(b2 ? b3))
;; => ((B2 LEFT-OF B3))
;; List the color of every block.
(fetch '(? color ?))
;; =>
;; ((B1 COLOR GREEN) (B2 COLOR RED) (B3 COLOR RED) (B4 COLOR BLUE)
;;  (B5 COLOR GREEN) (B6 COLOR PURPLE))
;; What facts are known about block B4?
(fetch '(b4 ? ?))
;; =>
;; ((B4 SHAPE PYRAMID) (B4 COLOR BLUE) (B4 SIZE LARGE) (B4 SUPPORTED-BY B5))

;; e)
(defun block-color (block)
  (list block 'color '?))

(block-color 'b3)
;; => (B3 COLOR ?)

;; f)
(defun supporters (block)
  (mapcar #'car (fetch (list '? 'supports block))))

(supporters 'b1)
;; => (B2 B3)

;; g)
(defun supp-cube (block)
  (let ((candidates (supporters block)))
    (every #'identity (mapcar #'(lambda (entry) (match-triple (list entry 'shape 'cube) (car (fetch (list entry 'shape '?))))) candidates))))

(supp-cube 'b4)
;; => T

(supp-cube 'b1)
;; => NIL

;; h)
(defun desc1 (block)
  (fetch (list block '? '?)))

(desc1 'b6)
;; => ((B6 SHAPE BRICK) (B6 COLOR PURPLE) (B6 SIZE LARGE))

;; i)
(defun desc2 (block)
  (mapcar #'cdr (desc1 block)))

(desc2 'b6)
;; => ((SHAPE BRICK) (COLOR PURPLE) (SIZE LARGE))

;; j)
(defun description (block)
  (reduce #'append (desc2 block)))

(description 'b6)
;; => (SHAPE BRICK COLOR PURPLE SIZE LARGE)

;; k)
(description 'b1)
;; => (SHAPE BRICK COLOR GREEN SIZE SMALL SUPPORTED-BY B2 SUPPORTED-BY B3)
(description 'b4)
;; => (SHAPE PYRAMID COLOR BLUE SIZE LARGE SUPPORTED-BY B5)

;; l)
(append database '((b1 material wood) (b2 material plastic)))
;; =>
;; ((B1 SHAPE BRICK) (B1 COLOR GREEN) (B1 SIZE SMALL) (B1 SUPPORTED-BY B2)
;;  (B1 SUPPORTED-BY B3) (B2 SHAPE BRICK) (B2 COLOR RED) (B2 SIZE SMALL)
;;  (B2 SUPPORTS B1) (B2 LEFT-OF B3) (B3 SHAPE BRICK) (B3 COLOR RED)
;;  (B3 SIZE SMALL) (B3 SUPPORTS B1) (B3 RIGHT-OF B2) (B4 SHAPE PYRAMID)
;;  (B4 COLOR BLUE) (B4 SIZE LARGE) (B4 SUPPORTED-BY B5) (B5 SHAPE CUBE)
;;  (B5 COLOR GREEN) (B5 SIZE LARGE) (B5 SUPPORTS B4) (B6 SHAPE BRICK)
;;  (B6 COLOR PURPLE) (B6 SIZE LARGE) (B1 MATERIAL WOOD) (B2 MATERIAL PLASTIC))

;; 7.30
(setf words '((one un)
              (two deux)
              (three trois)
              (four quatre)
              (five cinq)))

(mapcar #'nconc words '(uno dos tres quatro cinco))
