;; 3.1
(not (equal 3 (abs -3)))
;; => NIL

;; 3.2
(/ (+ 8 12) 2)
;; => 10 (4 bits, #xA, #o12, #b1010)

;; 3.3
(+ (* 3 3) (* 4 4))
;; => 25 (5 bits, #x19, #o31, #b11001)

;; 3.5
(defun half (n)
  (/ n 2))

(half 4)
;; => 2 (2 bits, #x2, #o2, #b10)

(defun cube (n)
  (* n n n))

(cube 4)
;; => 64 (7 bits, #x40, #o100, #b1000000)

(defun onemorep (x y)
  (equal x (+ y 1)))

(onemorep 2 1)
;; => T
(onemorep 3 7)
;; => NIL

;; 3.6
(defun pythag (x y)
  (sqrt (+ (* x x) (* y y))))

(pythag 3 4)
;; => 5.0

;; 3.7
(defun miles-per-gallon (initial-odometer-reading final-odometer-reading gallons-consumed)
  (/ (- final-odometer-reading initial-odometer-reading) gallons-consumed))

(miles-per-gallon 450 674 30)
;; => 112/15 (7.4666667)

;; 3.9
(cons 5 (list 6 7))
;; => (5 6 7)
(cons 5 '(list 6 7))
;; => (5 LIST 6 7)
(list 3 'from 9 'gives (- 9 3))
;; => (3 FROM 9 GIVES 6)
(+ (length '(1 foo 2 moo))
   (third '(1 foo 2 moo)))
;; => 6 (3 bits, #x6, #o6, #b110)

;; 3.10
;; (third (the quick brown fox)) missing quote
(third '(the quick brown fox))
;; => BROWN
;; (list 2 and 2 is 4) missing quotes
(list 2 'and 2 'is 4)
;; => (2 AND 2 IS 4)
;; (+ 1 '(length (list t t t t))) wrong quote
(+ 1 (length (list t t t t)))
;; => 5 (3 bits, #x5, #o5, #b101)
;; (cons 'patrick (seymour marvin)) missing quote
(cons 'patrick '(seymour marvin))
;; => (PATRICK SEYMOUR MARVIN)
;; (cons 'patrick (list seymour marvin)) missing quotes
(cons 'patrick (list 'seymour 'marvin))
;; => (PATRICK SEYMOUR MARVIN)

;; 3.11
(defun longer-than-p (list1 list2)
  (when (> (length list1) (length list2))
    t))

(longer-than-p '(a b c d) '(a b c))
;; => T
(longer-than-p '(a b c) '(a b c d))
;; => NIL

;; 3.12
(defun addlength (l)
  (cons (length l) l))

(addlength '(moo goo gai pan))
;; => (4 MOO GOO GAI PAN)
(addlength (addlength '(a b c)))
;; => (4 3 A B C)

;; 3.15
(defun scrabble (word)
  (list word 'is 'a 'word))

(scrabble 'aardvark)
;; => (AARDVARK IS A WORD)
(scrabble 'word)
;; => (WORD IS A WORD)

;; 3.16
(defun stooge (larry moe curly)
  (list larry (list 'moe curly) curly 'larry))

(stooge 'moe 'curly 'larry)
;; => (MOE (MOE LARRY) LARRY LARRY)

;; 3.19
(cons 'grapes '(of wrath))
;; => (GRAPES OF WRATH)
(list t 'is 'not nil)
;; => (T IS NOT NIL)
(first '(list moose goose))
;; => LIST
(first (list 'moose 'goose))
;; => MOOSE
(cons 'home ('sweet 'home))
;; => illegal function call

;; 3.20
(defun mystery (x)
  (list (second x) (first x)))

(mystery '(dancing bear))
;; => (BEAR DANCING)
(mystery 'dancing 'bear) ;; invalid number of arguments
(mystery '(zowie))
;; => (NIL ZOWIE)
(mystery (list 'first 'second))
;; => (SECOND FIRST)

;; 3.22
;; c)
(defun myfun (x y)
  (list (list x) y))

(myfun 'alpha 'beta)
;; => ((ALPHA) BETA)

;; d)
(defun firstp (s l)
  (when (equal s (first l))
    t))

(firstp 'foo '(foo bar baz))
;; => T
(firstp 'boing '(foo bar baz))
;; => NIL

;; e)
(defun mid-add1 (l)
  (list (first l)
        (+ 1 (second l))
        (third l)))

(mid-add1 '(take 2 cookies))
;; => (TAKE 3 COOKIES)

;;f)
(defun f-to-c (x)
  (/ (* (- x 32) 5) 9))

(f-to-c 65)
;; => 55/3 (18.333334)

;; 3.23
(lambda (x) (* x 2))
;; => #<FUNCTION (LAMBDA (X)) {52D1037B}>
(lambda (x) (* x x ))
;; => #<FUNCTION (LAMBDA (X)) {52D1041B}>
(lambda (x y) (equal x (+ y 1)))
;; => #<FUNCTION (LAMBDA (X Y)) {52D104BB}>

;; 3.25
(list 'cons t nil)
;; => (CONS T NIL)
(eval (list 'cons t nil))
;; => (T)
(eval (eval (list 'cons t nil)))
(apply #'cons '(t nil))
;; => (T)
(eval nil)
;; => NIL
(list 'eval nil)
;; => (EVAL NIL)
(eval (list 'eval nil))
;; => NIL
