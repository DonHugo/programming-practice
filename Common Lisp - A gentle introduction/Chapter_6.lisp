;; 6.5
(setf line '(roses are red))
;; => (ROSES ARE RED)

(reverse line)
;; => (RED ARE ROSES)

(first (last line))
;; => RED

(nth 1 line)
;; => ARE

(reverse (reverse line))
;; => (ROSES ARE RED)

(append line (list (first line)))
;; => (ROSES ARE RED ROSES)

(append (last line) line)
;; => (RED ROSES ARE RED)

(list (first line) (last line))
;; => (ROSES (RED))

(cons (last line) line)
;; => ((RED) ROSES ARE RED)

(remove 'are line)
;; => (ROSES RED)

(append line '(violets are blue))
;; => (ROSES ARE RED VIOLETS ARE BLUE)

;; 6.6
;; using reverse
(defun last-element (l)
  (nth 0 (reverse l)))

(last-element line)
;; => RED

;; using nth and length
(defun last-element (l)
  (nth (- (length line) 1) l))

(last-element line)
;; => RED

;; 6.7
(defun next-to-last (l)
  (nth 1 (reverse l)))

(next-to-last line)
;; => ARE

;; 6.8
(defun my-butlast (l)
  (reverse (cdr (reverse l))))

(my-butlast line)
;; => (ROSES ARE)
(my-butlast '(G A G A))
;; => (G A G)

;; 6.10
(defun palindromep (l)
  (if (equal l (reverse l))
      t
      nil))

(palindromep '(a b c d c b a))
;; => T
(palindromep '(a b c a b c))
;; => NIL

;; 6.11
(defun make-palindrome (l)
  (append l (reverse l)))

(make-palindrome '(you and me))
;; => (YOU AND ME ME AND YOU)

;; 6.15
(defun contains-the-p (sent)
  (member 'the sent))

(contains-the-p '(it is the pizza))
;; => (THE PIZZA)

(defun contains-article-p (sent)
  (if (intersection '(a an the) sent)
      t
      nil))

(contains-article-p '(a very nice pizza is the best thing))
;; => T

(defun contains-article-p (sent)
  (if (or (member 'a sent)
          (member 'an sent)
          (member 'the sent))
      t
      nil))

(contains-article-p '(the very nice pizza is best thing))
;; => T

;; 6.18
(defun add-vowels (letters)
  (union letters '(a e i o u)))

(add-vowels '(x a e z))
;; => (U O I X A E Z)

;; 6.21
(defun my-subsetp (s1 s2)
  (if (set-difference s1 s2)
      nil
      t))

(my-subsetp '(a b c) '(a b c d))
;; => T
(my-subsetp '(a b c) '(a b))
;; => NIL

;; 6.22
(setf a '(soap water))

(union a '(no soap radio))
;; => (RADIO NO SOAP WATER)
(intersection a (reverse a))
;; => (WATER SOAP)
(set-difference a '(stop for water))
;; => (SOAP)
(set-difference a a)
;; => NIL
(member 'soap a)
;; => (SOAP WATER)
(member 'water a)
;; => (WATER)
(member 'washcloth a)
;; => NIL

;; 6.24
(defun set-equal (s1 s2)
  (if (and (not (set-difference s1 s2))
           (not (set-difference s2 s1)))
      t
      nil))

(set-equal '(a b c d) '(a b c d))
;; => T
(set-equal '(red blue green) '(green blue red))
;; => T
(set-equal '(a f e d) '(a b e e))
;; => NIL

;; 6.25
(defun proper-subsetp (s1 s2)
  (if (and (subsetp s1 s2)
           (not (and (not (set-difference s1 s2))
                     (not (set-difference s2 s1)))))
      t
      nil))

(proper-subsetp '(a c) '(c a b))
;; => T
(proper-subsetp '(a b c) '(c a b))
;; => NIL

;; 6.26
(setf a '(large red shiny cube -vs- small shiny red four-sided pyramid))
;; => (LARGE RED SHINY CUBE -VS- SMALL SHINY RED FOUR-SIDED PYRAMID)

;; a)
(defun right-side (l)
  "Returns the right side of a '-vs-' separated list."
  (cdr (member '-vs- l)))

(right-side a)
;; => (SMALL SHINY RED FOUR-SIDED PYRAMID)

;; b)
(defun left-side (l)
  "Returns the left side of a '-vs-' separated list."
  (cdr (member '-vs- (reverse l))))

(left-side a)
;; => (CUBE SHINY RED LARGE)

;; c)
(defun count-common (l)
  "Counts common elements in a '-vs-' separated list."
  (length (intersection (left-side l) (right-side l))))

(count-common a)
;; => 2 (2 bits, #x2, #o2, #b10)

;; d)
(defun compare (l)
  "Returns number of common elements in a '-vs-' separated list."
  (list (count-common l) 'common 'features))

(compare a)
;; => (2 COMMON FEATURES)

;; e)
(compare '(small red metal cube -vs- red plastic small cube))
;; => (3 COMMON FEATURES)

;; 6.28
(setf produce '((apple . fruit)
                (celery . veggie)
                (banana . fruit)
                (lettuce . veggie)))

(assoc 'banana produce)
;; => (BANANA . FRUIT)
(rassoc 'fruit produce)
;; => (APPLE . FRUIT)
(assoc 'lettuce produce)
;; => (LETTUCE . VEGGIE)
(rassoc 'veggie produce)
;; => (CELERY . VEGGIE)

;; 6.30
(setf books '((war-and-peace . leo-tolstoy)
              (der-gesang-der-flusskrebse . renate-bergmann)
              (die-tribute-von-panem . suzanne-collins)
              (geheime-quellen . donna-leon)
              (wenn-du-zurückkehrst . nicholas-sparks)))
;; =>
;; ((WAR-AND-PEACE . LEO-TOLSTOY) (DER-GESANG-DER-FLUSSKREBSE . RENATE-BERGMANN)
;;  (DIE-TRIBUTE-VON-PANEM . SUZANNE-COLLINS) (GEHEIME-QUELLEN . DONNA-LEON)
;;  (WENN-DU-ZURÜCKKEHRST . NICHOLAS-SPARKS))

;; 6.31
(defun who-wrote (book)
  (cdr (assoc book books)))

(who-wrote 'war-and-peace)
;; => LEO-TOLSTOY

;; 6.32
(setf books (reverse books))
(who-wrote 'war-and-peace)
;; => LEO-TOLSTOY

;; 6.34
(setf atlas '((pennsylvania pittsburgh)
              (new-jersey newark)
              (pennsylvania johnstown)
              (ohio columbus)
              (new-jersey princeton)
              (new-jersey trenton)))
;; =>
;; ((PENNSYLVANIA PITTSBURGH) (NEW-JERSEY NEWARK) (PENNSYLVANIA JOHNSTOWN)
;;  (OHIO COLUMBUS) (NEW-JERSEY PRINCETON) (NEW-JERSEY TRENTON))

(assoc 'new-jersey atlas)
;; => (NEW-JERSEY NEWARK)

(setf atlas '((pennsylvania '(pittsburgh johnstown))
              (ohio columbus)
              (new-jersey '(newark princeton trenton))))

(assoc 'new-jersey atlas)
;; => (NEW-JERSEY '(NEWARK PRINCETON TRENTON))

;; 6.35
;; a)
(setf nerd-states '((sleeping . eating)
                    (eating . waiting-for-a-computer)
                    (waiting-for-a-computer . programming)
                    (programming . debugging)
                    (debugging . sleeping)))
;; =>
;; ((SLEEPING . EATING) (EATING . WAITING-FOR-A-COMPUTER)
;;  (WAITING-FOR-A-COMPUTER . PROGRAMMING) (PROGRAMMING . DEBUGGING)
;;  (DEBUGGING . SLEEPING))

;; b)
(defun nerdus (state)
  (cdr (assoc state nerd-states)))

(nerdus 'sleeping)
;; => EATING
(nerdus 'debugging)
;; => SLEEPING

;; c)
(nerdus 'playing-guitar)
;; => NIL

;; d)
(defun sleepless-nerd (state)
  (let ((new-state (cdr (assoc state nerd-states))))
    (if (equal new-state 'sleeping)
        (cdr (assoc new-state nerd-states))
        new-state)))

(sleepless-nerd 'programming)
;; => DEBUGGING
(sleepless-nerd 'debugging)
;; => EATING

;; e)
(defun nerd-on-caffeine (state)
  (cdr (assoc (cdr (assoc state nerd-states)) nerd-states)))

(nerd-on-caffeine 'eating)
;; => PROGRAMMING
(nerd-on-caffeine 'programming)
;; => SLEEPING

;; 6.36
(defun swap-first-last (l)
  (append (cdr l) (list (car l))))

(swap-first-last '(you cant buy love))
;; => (CANT BUY LOVE YOU)

;; 6.37
(defun rotate-left (l)
  (append (cdr l) (list (car l))))

(rotate-left '(a b c d e))
;; => (B C D E A)

(defun rotate-right (l)
  (append (list (car (reverse l)))
          (reverse (cdr (reverse l)))))

(rotate-right '(a b c d e))
;; => (E A B C D)

;; 6.41
(defvar rooms
  '((living-room
     (north front-stairs)
     (south dining-room)
     (east kitchen))

    (upstairs-bedroom
     (west library)
     (south front-stairs))

    (dining-room
     (north living-room)
     (east pantry)
     (west downstairs-bedroom))

    (kitchen
     (west living-room)
     (south pantry))

    (pantry
     (north kitchen)
     (west dining-room))

    (downstairs-bedroom
     (north back-stairs)
     (east dining-room))

    (back-stairs
     (south downstairs-bedroom)
     (north library))

    (front-stairs
     (north upstairs-bedroom)
     (south living-room))

    (library
     (east upstairs-bedroom)
     (south back-stairs))))

;; a)
(defun choices (room)
  (cdr (assoc room rooms)))

(choices 'pantry)
;; => ((NORTH KITCHEN) (WEST DINING-ROOM))

;; b)
(defun look (direction room)
  (car (cdr (assoc direction (choices room)))))

(look 'north 'pantry)
;; => KITCHEN
(look 'west 'pantry)
;; => DINING-ROOM
(look 'south 'pantry)
;; => NIL

;; c)
(defvar loc 'pantry)

(defun set-robbie-location (place)
  "Moves Robbie to PLACE by setting the variable LOC"
  (setf loc place))

;; d)
(defun how-many-choices (loc)
  (length (choices loc)))

(how-many-choices 'pantry)
;; => 2 (2 bits, #x2, #o2, #b10)

;; e)
(defun upstairsp (location)
  (if (or (equal location 'upstairs-bedroom)
          (equal location 'library))
      t
      nil))

(defun onstairsp (location)
  (if (or (equal location 'front-stairs)
          (equal location 'back-stairs))
      t
      nil))

;; f)
(defun where ()
  (cond ((upstairsp loc) (list 'robbie 'is 'upstairs 'in 'the loc))
        ((onstairsp loc) (list 'robbie 'is 'on 'the loc))
        (t (list 'robbie 'is 'downstairs 'in 'the loc))))

(where)
;; => (ROBBIE IS DOWNSTAIRS IN THE PANTRY)

;; g)
(defun move (direction)
  (if (look direction loc)
      (and (setf loc (look direction loc))
           (where))
      (list 'ouch! 'robbie 'hit 'a 'wall)))

(move 'south)
;; => (OUCH! ROBBIE HIT A WALL)

(move 'north)
;; => (ROBBIE IS DOWNSTAIRS IN THE KITCHEN)

;; h)
(setf loc 'pantry)
;; => PANTRY

(move 'west)
;; => (ROBBIE IS DOWNSTAIRS IN THE DINING-ROOM)
(move 'west)
;; => (ROBBIE IS DOWNSTAIRS IN THE DOWNSTAIRS-BEDROOM)
(move 'north)
;; => (ROBBIE IS ON THE BACK-STAIRS)
(move 'north)
;; => (ROBBIE IS UPSTAIRS IN THE LIBRARY)

(move 'east)
;; => (ROBBIE IS UPSTAIRS IN THE UPSTAIRS-BEDROOM)
(move 'south)
;; => (ROBBIE IS ON THE FRONT-STAIRS)
(move 'south)
;; => (ROBBIE IS DOWNSTAIRS IN THE LIVING-ROOM)
(move 'east)
;; => (ROBBIE IS DOWNSTAIRS IN THE KITCHEN)

;; 6.42
(defun royal-we (l)
  "Change every occurence of I wo WE in list L."
  (subst 'we 'I l))

(royal-we '(If I learn Lisp I will be pleased))
;; => (IF WE
;;     LEARN
;;     LISP
;;     WE
;;     WILL
;;     BE
;;     PLEASED)
