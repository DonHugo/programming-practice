;; 4.1
(defun make-even (x)
  (if (evenp x)
      x
      (+ x 1)))

(make-even 1)
;; => 2 (2 bits, #x2, #o2, #b10)
(make-even 2)
;; => 2 (2 bits, #x2, #o2, #b10)

;; 4.2
(defun further (x)
  (if (plusp x)
      (+ x 1)
      (if (minusp x)
          (- x 1)
          x)))

(further 12)
;; => 13 (4 bits, #xD, #o15, #b1101)
(further -45)
;; => -46 (6 bits)
(further 0)
;; => 0 (0 bits, #x0, #o0, #b0)

;; 4.3
(defun my-not (x)
  (if x
      nil
      t))

(my-not t)
;; => NIL
(my-not nil)
;; => T

;; 4.4
(defun ordered (x y)
  (if (< x y)
      (list x y)
      (list y x)))

(ordered 3 4)
;; => (3 4)
(ordered 4 3)
;; => (3 4)

;; 4.6
(defun my-abs (x)
  (cond ((< x 0) (- x))
        (t x)))

(my-abs 5)
;; => 5 (3 bits, #x5, #o5, #b101)
(my-abs -5)
;; => 5 (3 bits, #x5, #o5, #b101)

;; 4.8
(defun emphasize3 (x)
  (cond ((equal (first x) 'good) (cons 'great (rest x)))
         ((equal (first x) 'bad) (cons 'awful (rest x)))
         (t (cons 'very x))))

(emphasize3 '(long day))
;; => (VERY LONG DAY)
(emphasize3 '(very long day))
;; => (VERY VERY LONG DAY)

;; 4.9
(defun make-odd (x)
  (cond ((not (oddp x)) (+ x 1))
        (t x)))

(make-odd 3)
;; => 3 (2 bits, #x3, #o3, #b11)
(make-odd 4)
;; => 5 (3 bits, #x5, #o5, #b101)
(make-odd -2)
;; => -1 (0 bits)

;; 4.10
(defun constrain (x max min)
  (cond ((< x min) min)
        ((> x max) max)
        (t x)))

(constrain 3 50 -50)
;; => 3 (2 bits, #x3, #o3, #b11)
(constrain 92 50 -50)
;; => 50 (6 bits, #x32, #o62, #b110010)

;; 4.11
(defun firstzero (l)
  (cond ((equal (first l) 0) 'first)
        ((equal (second l) 0) 'second)
        ((equal (third l) 0) 'third)
        (t 'none)))

(firstzero '(3 0 4))
;; => SECOND
(firstzero '(0 2 3))
;; => FIRST
(firstzero '(3 1 4))
;; => NONE

;; 4.12
(defun cycle (x)
  (cond ((= x 99) 1)
        (t (+ x 1))))

(cycle 1)
;; => 2 (2 bits, #x2, #o2, #b10)
(cycle 98)
;; => 99 (7 bits, #x63, #o143, #b1100011)
(cycle 99)
;; => 1 (1 bit, #x1, #o1, #b1)

;; 4.13
(defun howcompute (x y res)
  (cond ((equal res (+ x y)) 'sum-of)
        ((equal res (* x y)) 'product-of)
        ((equal res (- x y)) 'substraction-of)
        ((equal res (/ x y)) 'division-of)
        (t '(beats me))))

(howcompute 3 4 12)
;; => PRODUCT-OF
(howcompute 3 4 200)
;; => (BEATS ME)

;; 4.15
(defun geq (x y)
  (or (> x y) (= x y)))

(geq 1 1)
;; => T
(geq 2 1)
;; => T
(geq 1 2)
;; => NIL

;; 4.16
(defun mymathfun (x)
  (cond ((and (oddp x) (plusp x)) (* x x))
        ((and (oddp x) (minusp x)) (* x 2))
        (t (/ x 2))))

(mymathfun 3)
;; => 9 (4 bits, #x9, #o11, #b1001)
(mymathfun -3)
;; => -6 (3 bits)
(mymathfun 10)
;; => 5 (3 bits, #x5, #o5, #b101)

;; 4.17
(defun mypredp (gender mature)
  (or (and (or (equal gender 'boy)
               (equal gender 'girl))
           (equal mature 'child))
      (and (or (equal gender 'man)
               (equal gender 'woman))
           (equal mature 'adult))))

(mypredp 'girl 'child)
;; => T
(mypredp 'woman 'adult)
;; => T
(mypredp 'boy 'adult)
;; => NIL

;; 4.18
(defun play (p1 p2)
  (cond ((and (equal p1 'scissors) (equal p2 'scissors)) 'tie)
        ((and (equal p1 'rock) (equal p2 'rock)) 'tie)
        ((and (equal p1 'paper) (equal p2 'paper)) 'tie)
        ;; p1
        ((or (and (equal p1 'scissors) (equal p2 'paper))
             (and (equal p1 'rock) (equal p2 'scissors))
             (and (equal p1 'paper) (equal p2 'rock))) 'first-win)
        ;; p2
        ((or (and (equal p2 'scissors) (equal p1 'paper))
             (and (equal p2 'rock) (equal p1 'scissors))
             (and (equal p2 'paper) (equal p1 'rock))) 'second-win)
        (t 'WTF?!)))


(play 'scissors 'rock)
;; => SECOND-WIN
(play 'scissors 'paper)
;; => FIRST-WIN
(play 'banana 'apple)
;; => WTF?!
