;; 8.3
(defun fact (n)
  (cond ((zerop n) 1)
        (t (* n (fact (- n 1))))))

(fact 5)
;; => 120 (7 bits, #x78, #o170, #b1111000)
(fact 20)
;; => 2432902008176640000 (62 bits, #x21C3677C82B40000)
(fact 20.0)
;; => 2.432902e18
(fact 0)
;; => 1 (1 bit, #x1, #o1, #b1)
(fact 0.0)
;; => 1 (1 bit, #x1, #o1, #b1)

;; 8.4
(defun laugh (n)
  "Laugh n times."
  (cond ((>= 0 n) nil)
        (t (cons 'ha (laugh (- n 1))))))

(laugh 5)
;; => (HA HA HA HA HA)
(laugh 0)
;; => NIL
(Laugh -1)
;; => NIL

;; 8.5
(defun add-up (numbers)
  "Add up numbers (a list)"
  (cond ((eq numbers '()) 0)
        (t (+ (car numbers)
              (add-up (rest numbers))))))

(add-up '(2 3 7))
;; => 12 (4 bits, #xC, #o14, #b1100)

;; 8.6
(defun alloddp (numbers)
  "Return t if all numbers are odd."
  (cond ((eq numbers '()) t) ; if list is empty, all numbers have been odd
        ((evenp (car numbers)) nil)
        (t (alloddp (rest numbers)))))

(alloddp '(1 2 3))
;; => NIL
(alloddp '(1 1 1))
;; => T

;; 8.7
(defun rec-member (item list)
  (cond ((eq list '()) nil)
        ((equal item (car list)) list)
        (t (rec-member item (rest list)))))

(member 8 '(1 2 3 8 9 19 1222))
;; => (8 9 19 1222)
(rec-member 8 '(1 2 3 8 9 19 1222))
;; => (8 9 19 1222)
(rec-member 10 '(1 2 3 8 9 19 1222))
;; => NIL

;; 8.8
(defun rec-assoc (item alist)
  (cond ((eq alist '()) nil)
        ((equal item (caar alist)) (car alist))
        (t (rec-assoc item (rest alist)))))

(assoc 'banana '((banana fruit)
                 (lemon fruit)))
;; => (BANANA FRUIT)
(rec-assoc 'banana '((banana fruit)
                     (lemon fruit)))
;; => (BANANA FRUIT)

;; 8.9
(defun rec-nth (n list)
  (cond ((eq list '()) nil)
        ((= n 0) (car list))
        (t (rec-nth (- n 1) (cdr list)))))

(nth 4 '(1 2 3 4 5 6 7 8))
;; => 5 (3 bits, #x5, #o5, #b101)
(rec-nth 4 '(1 2 3 4 5 6 7 8))
;; => 5 (3 bits, #x5, #o5, #b101)
(rec-nth 4 '())
;; => NIL
(rec-nth 4 '(1 2 3))
;; => NIL
(rec-nth 1 '(1 2 3))
;; => 2 (2 bits, #x2, #o2, #b10)

;; 8.10
(defun add1 (n)
  "Add 1 to N."
  (+ n 1))

(defun sub1 (n)
  "Substract 1 to N."
  (- n 1))

(defun rec-plus (x y)
  (cond ((zerop y) x)
        (t (rec-plus (add1 x) (sub1 y)))))

(rec-plus 4 3)
;; => 7 (3 bits, #x7, #o7, #b111)
(rec-plus 1 'me)
;; => 2 (2 bits, #x2, #o2, #b10)

;; 8.11
(defun fib (n)
  (cond ((zerop n) 1)
        ((= 1 n) 1)
        (t (+ (fib (- n 1))
              (fib (- n 2))))))

(fib 4)
;; => 5 (3 bits, #x5, #o5, #b101)
(fib 5)
;; => 8 (4 bits, #x8, #o10, #b1000)

;; 8.12
(defun any-7-p (x)
  (cond ((equal (first x) 7) t)
        (t (any-7-p (rest x)))))

(any-7-p '(1 23 7 8 9))
;; => T
;; (any-7-p '(1 23 10 8 9))
;; Inf Rec

;; 8.17
(defun find-first-odd (list)
  (cond ((null list) nil)
        ((oddp (first list)) (first list))
        (t (find-first-odd (rest list)))))

(find-first-odd '(5 1 2 3 4 5 6))
;; => 5 (3 bits, #x5, #o5, #b101)

;; 8.18
(defun last-element (list)
  (cond ((atom (cdr list)) (car list))
        (t (last-element (cdr list)))))

(last-element '((((a f)) i) r))
;; => R
(last-element '(ooh ah eee))
;; => EEE

;; 8.20
(defun add-nums (number)
  (cond ((zerop number) number)
        (t (+ number (add-nums (- number 1))))))

(add-nums 5)
;; => 15 (4 bits, #xF, #o17, #b1111)
(add-nums 394)
;; => 77815 (17 bits, #x12FF7)

(defun all-equal (list)
  (cond ((null (rest list)) t)
        ((not (equal (first list) (second list))) nil)
        (t (all-equal (rest list)))))

(all-equal '(a a a))
;; => T
(all-equal '(1 1 1))
;; => T
(all-equal '(1 2 3))
;; => NIL

;; 8.24
(defun count-down (n)
  (cond ((zerop n) nil)
        (t (cons n (count-down (- n 1))))))

(count-down 5)
;; => (5 4 3 2 1)

;; 8.25
(defun fact (n)
  (reduce #'* (count-down n)))

(fact 5)
;; => 120 (7 bits, #x78, #o170, #b1111000)

;; 8.26
(defun count-down (n)
  (cond ((zerop n) (list 0))
        (t (cons n (count-down (- n 1))))))

(count-down 5)
;; => (5 4 3 2 1 0)

(defun count-down (n)
  (cond ((equal n -1) nil)
        (t (cons n (count-down (- n 1))))))

(count-down 5)
;; => (5 4 3 2 1 0)

;; 8.27
(defun square-list (numbers)
  (cond ((null numbers) nil)
        (t (cons (* (car numbers)
                    (car numbers))
                 (square-list (rest numbers))))))

(square-list '(3 4 5 6))
;; => (9 16 25 36)

;; 8.28
(defun my-nth (n x)
  (cond ((null x) nil)
        ((zerop n) (first x))
        (t (my-nth (- n 1) (rest x)))))

(my-nth 5 '(A B C))
;; => NIL
(my-nth 1000 '(A B C))
;; => NIL

;; 8.29
(defun my-member (x list)
  (cond ((null list) nil)
        ((equal (car list) x) list)
        (t (my-member x (cdr list)))))

(my-member 1 '(2 3 5 6 9 7 6))
;; => NIL
(my-member 5 '(2 3 5 6 9 7 6))
;; => (5 6 9 7 6)

;; 8.30
(defun my-assoc (item alist)
  (cond ((null alist) nil)
        ((equal item (caar alist)) (car alist))
        (t (my-assoc item (rest alist)))))

(my-assoc 'banana '((banana fruit)
                    (lemon fruit)))
;; => (BANANA FRUIT)

;; 8.31
(defun compare-lengths (list1 list2)
  (cond ((and (null list1) (null list2)) 'same-length)
        ((null list1) 'second-is-longer)
        ((null list2) 'first-is-longer)
        (t (compare-lengths (cdr list1) (cdr list2)))))

(compare-lengths '(a b c d e f) '(a b e d f g j u i o k m n h))
;; => SECOND-IS-LONGER
(compare-lengths '(a b c d e f) '(a b e d f g))
;; => SAME-LENGTH

;; 8.32
(defun sum-numeric-elements (list)
  (cond ((null list) 0)
        ((numberp (first list))
         (+ (first list)
            (sum-numeric-elements (rest list))))
        (t (sum-numeric-elements (rest list)))))

(sum-numeric-elements '(3 bears 3 bowls and 1 girl))

;; 8.33
(defun my-remove (item list)
  (cond ((null list) nil)
        ((equal item (first list))
         (my-remove item (rest list)))
        (t (cons (first list) (my-remove item (rest list))))))

(remove 'b '(a b c))
;; => (A C)
(my-remove 'b '(a b c))
;; => (A C)

;; 8.34
(defun my-intersection (list1 list2)
  (cond ((or (null list1) (null list2)) nil)
        ((member (first list1) list2)
         (cons (first list1) (my-intersection (cdr list1) list2)))
        (t (my-intersection (rest list1) list2))))

(intersection '(1 2 3 4) '(2 3 4 5))
;; => (4 3 2)
(my-intersection '(1 2 3 4) '(2 3 4 5))
;; => (2 3 4)

;; 8.35
(defun my-set-difference (list1 list2)
  (cond ((null list1) nil)
        ((member (first list1) list2)
         (my-set-difference (rest list1) list2))
        (t (cons (first list1) (my-set-difference (rest list1) list2)))))

(set-difference '(1 2 3 4) '(2 3 4 5))
;; => (1)
(my-set-difference '(1 2 3 4) '(2 3 4 5))
;; => (1)

;; 8.36
;; conditional augmentation
(defun count-odd (numbers)
  (cond ((null numbers) 0)
        ((oddp (first numbers))
         (+ 1 (count-odd (rest numbers))))
        (t (count-odd (rest numbers)))))

(count-odd '(4 5 6 7 8))
;; => 2 (2 bits, #x2, #o2, #b10)

;; regular augmenting recursion
(defun count-odd (numbers)
  (cond ((null numbers) 0)
        (t (+ (if (oddp (first numbers))
                  1
                  0)
              (count-odd (rest numbers))))))

(count-odd '(4 5 6 7 8))
;; => 2 (2 bits, #x2, #o2, #b10)

;; 8.39
(defun count-atoms (x)
  (cond ((atom x) 1)
        ((null x) 1)
        (t (+ (count-atoms (car x))
              (count-atoms (cdr x))))))

(count-atoms '(a (b) c))
;; => 5 (3 bits, #x5, #o5, #b101)
(count-atoms '(nil 1 2 3 (3 4 5 (6))))
;; => 11 (4 bits, #xB, #o13, #b1011)

;;. 8.40
(defun count-cons (x)
  (cond ((atom x) 0)
        (t (+ 1
              (count-cons (car x))
              (count-cons (cdr x))))))

(count-cons '(foo))
;; => 1 (1 bit, #x1, #o1, #b1)
(count-cons '(foo bar))
;; => 2 (2 bits, #x2, #o2, #b10)
(count-cons '((foo)))
;; => 2 (2 bits, #x2, #o2, #b10)
(count-cons 'fred)
;; => 0 (0 bits, #x0, #o0, #b0)

;; 8.41
(defun sum-tree (tree)
  (cond ((numberp tree) tree)
        ((null tree) 0)
        ((atom tree) 0)
        (t (+ (sum-tree (car tree))
              (sum-tree (cdr tree))))))

(sum-tree '((3 bears) (3 bowls) (1 girl)))
;; => 7 (3 bits, #x7, #o7, #b111)

;; 8.42
(defun my-subst (new old tree)
  (cond ((equal tree old) new)
        ((atom tree) tree)
        (t (cons (my-subst new old (car tree))
                 (my-subst new old (cdr tree))))))

(subst 'a 'b '(a b c (a b c) ((b c))))
;; => (A A C (A A C) ((A C)))
(my-subst 'a 'b '(a b c (a b c) ((b c))))
;; => (A A C (A A C) ((A C)))

;; 8.43
(defun flatten (tree)
  (cond ((atom tree) (list tree))
        (t (append (flatten (car tree))
                   (when (cdr tree) (flatten (cdr tree)))))))

(flatten '((a b (r)) a c (a d ((a (b)) r) a)))
;; => (A B R A C A D A B R A)

;; 8.44
(defun tree-depth (tree)
  (cond ((atom tree) 0)
        (t (+ 1 (max (tree-depth (car tree))
                     (tree-depth (cdr tree)))))))

(tree-depth '((a b c d)))
;; => 5 (3 bits, #x5, #o5, #b101)

;; 8.45
(defun paren-depth (tree)
  (cond ((atom tree) 0)
        (t (max (+ 1 (paren-depth (first tree)))
                (paren-depth (rest tree))))))

(paren-depth '(a b c))
;; => 1 (1 bit, #x1, #o1, #b1)
(paren-depth '(a b ((c) d) e))
;; => 3 (2 bits, #x3, #o3, #b11)

;; 8.46
(defun count-up (n)
  (cond ((zerop n) nil)
        (t (append (count-up (- n 1))
                   (list n)))))

(count-up 5)
;; => (1 2 3 4 5)
(count-up 15)
;; => (1 2 3 4 5 6 7 8 9 10 11 12 13 14 15)

;; 8.47
(defun make-loaf (n)
  (if (zerop n)
      nil
      (append (make-loaf (- n 1))
              (list 'X))))

(make-loaf 4)
;; => (X X X X)

;; 8.48
(defun bury (item n)
  (cond ((zerop n) item)
        (t (list (bury item (- n 1))))))

(bury 'fred 2)
;; => ((FRED))
(bury 'fred 5)
;; => (((((FRED)))))

;; 8.49
(defun pairings (list1 list2)
  (cond ((or (null list1) (null list2)) nil)
        (t (cons (list (first list1) (first list2))
                 (pairings (rest list1) (rest list2))))))

(pairings '(a b c) '(1 2 3))
;; => ((A 1) (B 2) (C 3))

;; 8.50
(defun sublists (x)
  (cond ((null x) nil)
        (t (cons x (sublists (cdr x))))))

(sublists '(fee fie foe))
;; => ((FEE FIE FOE) (FIE FOE) (FOE))

(defun my-reverse-helper (x y)
  (cond ((null x) y)
        (t (my-reverse-helper (rest x)
                              (cons (first x) y)))))

(defun my-reverse (x)
  (my-reverse-helper x nil))

(my-reverse '(a b c d e))
;; => (E D C B A)

;; 8.52
(defun my-union-helper (x y)
  (cond ((null y) nil)
        ((member (first y) x)
         (my-union-helper x (rest y)))
        (t (cons (first y)
                 (my-union-helper x (rest y))))))

(defun my-union (x y)
  (append x (my-union-helper x y)))

(my-union '(a b c) '(b c d e))
;; => (A B C D E)

;; 8.53
(defun largest-even (numbers)
  (cond ((null numbers) 0)
        ((evenp (first numbers))
         (max (first numbers) (largest-even (rest numbers))))
        (t (largest-even (rest numbers)))))

(largest-even '(5 2 4 3))
;; => 4 (3 bits, #x4, #o4, #b100)

;; 5.54
(defun huge-helper (number n)
  (cond ((zerop n) 1)
        (t (* number (huge-helper number (- n 1))))))

(defun huge (number)
  (huge-helper number number))

(huge 2)
;; => 4 (3 bits, #x4, #o4, #b100)
(huge 3)
;; => 27 (5 bits, #x1B, #o33, #b11011)
(huge 7)
;; => 823543 (20 bits, #xC90F7)

;; 8.57
(defun every-other (x)
  (cond ((null x) nil)
        (t (cons (first x) (every-other (cddr x))))))

(every-other '(a b c d e f g))
;; => (A C E G)
(every-other '(i came i saw i conquered))
;; => (I I I)

;; 8.58
(defun left-half-helper (x n)
  (cond ((not (plusp n)) nil)
        (t (cons (first x)
                 (left-half-helper (rest x) (- n 1))))))

(defun left-half (x)
  (left-half-helper x (/ (length x) 2)))

(left-half '(a b c d e))
;; => (A B C)

;; 8.59
(defun merge-lists (l1 l2)
  (cond ((null l1) l2)
        ((null l2) l1)
        ((< (first l1) (first l2))
         (cons (first l1)
               (merge-lists (rest l1) l2)))
        (t (cons (first l2)
                 (merge-lists l1 (rest l2))))))

(merge-lists '(1 2 6 8 10 12) '(2 3 5 9 13))
;; => (1 2 2 3 5 6 8 9 10 12 13)
